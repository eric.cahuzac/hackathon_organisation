# Semaine 5 - Hackathon Calcul-Stockage-Cloud
Cette dernière semaine sera vouée à :
* La rédaction du synthèse du hackathon
* La réflexion sur la gouvernance à mettre en place pour pérenniser le projet
* Définir ce qu'on va faire pour la suite (What’s next?)

Elle sera clôturée par une session en plénière le vendredi 26 juin de 10h à 12h30.

Pour permettre de fluidifier les échanges, nous avons prévu des moments d'échanges en live.
Les sessions du lundi seront agémentées avec des présentations de modalités de gouvernance collective par Lise Frappier, facilitatrice en intelligence collective.
- Lundi 22 juin : 
    - 9h-10h
    - 14h-15h
    - 17h-18h

- Mardi 23 juin et jeudi 25 juin : 
    - 14h-17h
