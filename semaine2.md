# Semaine 2 - Hackathon Calcul-Stockage-Cloud

**Du 02 juin 2020 au 05 juin 2020**

## Objectif 1 : s'organiser en axes d'intérêts 

On commence la semaine le Conseils de Lise sur l'auto-organisation des groupes en plénière le 2 juin de 09h à 10h :
  - sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#
  - Prise de notes : [https://etherpad.in2p3.fr/p/pcsc_lisehelp_202006020900](https://etherpad.in2p3.fr/p/pcsc_lisehelp_202006020900)
  - En dehors de ce créneau, vous pouvez toujours [poser vos questions à Lise](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/organisation) à tout moment.

Toujours le 2 juin, Tovo et alex répondes à toutes les questions de 10h à 12h30 et de 14h à18h :
  - sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#
  - prise de notes : [https://etherpad.in2p3.fr/p/pcsc_help_202006021000](https://etherpad.in2p3.fr/p/pcsc_help_202006021000)

Toute la matinée du jeudi 4 juin, Lise donne 30min de conseils d'organisation pour chaque groupe (8h30-12h30)
  - votez pour les créneaux de votre choix pour chacun des groupes auxquels vous désirez participer : [https://evento.renater.fr/survey/pcsc_lisehelp_orgaaxesdinteret-ieewqmhm](https://evento.renater.fr/survey/pcsc_lisehelp_orgaaxesdinteret-ieewqmhm)
  - les créneaux se suivent en continu, merci de vous connecter à l'heure précise de votre groupe
  - sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#
  - En dehors de ce créneau, vous pouvez toujours [poser vos questions à Lise](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/organisation) à tout moment.

Enfin, comme tous les vendredi, on se retrouve tous en Plénière de synthèse de fin de semaine de 11h à 12h
  - sur  https://meet.jit.si/pcsc ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#
  - prise de notes : https://etherpad.in2p3.fr/p/pcsc_point_semaine_2_202006051100






## Objectif 2 : documentation et recherche des compétences manquantes


Les objectifs de cette deuxième semaine sont :
  - Connaître l'état de l’art dans les outils d'aide à la décision, exemples : 
    - https://staging.gaia-x-demonstrator.eu/
    - https://choosealicense.com/
    - https://statindecisionaid.mayoclinic.org/
    - https://cat.opidor.fr/index.php/Cat_OPIDoR,_wiki_des_services_d%C3%A9di%C3%A9s_aux_donn%C3%A9es_de_la_recherche
    - https://fairsharing.org/
  - Portail XSEDE:
    - https://www.xsede.org/
    - https://portal.xsede.org/
  - Portail EOSC :
    - https://marketplace.eosc-portal.eu
    - https://marketplace.eosc-portal.eu/services/c/compute?q=&service_id=&sort=_score

    



## Les Axes d'Intérêts proposés
Suite à la semaine 1, 8 Axes d'Intérêt ont été identifiés :


##### Documentation 
  - Comment structurer la documentation ? 
  - Quelles informations y placer ?
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/documentation
  - https://etherpad.in2p3.fr/p/pcsc_axe_documentation_orga

##### Moteur OAD 
  - Apprentissage automatique de l’OAD à partir des fichiers de données.
  - Intégrer de la recherche libre et une carte interactive.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/oad
  - https://etherpad.in2p3.fr/p/pcsc_axe_oad_orga

##### UX User eXperience
  - Comment PCSC rendre opérationnel, intuitif et adapté à tous.
  - Un de peu design, c'est toujours bien aussi.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/ux
  - https://etherpad.in2p3.fr/p/pcsc_axe_ux_orga

##### Critères 
  - Quels sont les bons critères pour définir les 
    - utilisateurs
    - besoins
    - ressources (infra, services, accès)
    - recommandations ...
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/criteres
  - https://etherpad.in2p3.fr/p/pcsc_axe_criteres_orga

##### Ontologies 
  - Trouver/définir les vocabulaires de référence pour les critères et leurs liens entre eux et les concepts.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/ontologies
  - https://etherpad.in2p3.fr/p/pcsc_axe_ontologies_orga

##### Formats des données
  - Comment structurer les fichiers de données ?
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/formats
  - https://etherpad.in2p3.fr/p/pcsc_axe_formats_orga

##### Outils pour renseigner les données
  - Faciliter le travail de renseignement des données par les fournisseurs de ressources.
  - Les outils peuvent être graphiques ou non.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/outils-renseigner-donnees
  - https://etherpad.in2p3.fr/p/pcsc_axe_outils-renseigner-donnees_orga

##### Chatbot
  - Accompagnement plus simple pour les utilisateurs.
  - Recueil des questions des utilisateurs.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/chatbot
  - https://etherpad.in2p3.fr/p/pcsc_axe_chatbot_orga

##### API
  - Interagir avec d’autres portails, avec l’extérieur.
  - Pull / Push
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/api
  - https://etherpad.in2p3.fr/p/pcsc_axe_api_orga


