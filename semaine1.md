# Semaine 1 - Hackathon Portail Calcul-Stockage-Cloud
Le hackathon démarrera par des sessions de 1h en visio le lundi 25 mai. Pour permettre à tout le monde d’y participer, plusieurs créneaux sont proposés :

* 9h-10h
* 11h-12h
* 14h-15h
* 17h-18h

Si vous ne pouvez assister à aucune des visio du lundi 25 mai, envoyez un courriel aux [organisateurs](participants.md) afin de trouver un autre créneau. Sinon, venez mardi, mercredi ou jeudi.   


La semaine sera ponctuée de créneaux ouverts pour des échanges libres en visio. Une permanence sera assurée sur ces créneaux :
* mardi 26 : 9h-19h
* mercredi 27 : 9h-19h
* jeudi 28 : 9h-12h et 14h-18h

Venez 5min ou 2h, une seule fois ou tous les matins, midis et soirs de la semaine. C'est à la carte. De plus, des démonstrations de la forge seront organisées sur demande.

La semaine se clôturera par une session plénière le vendredi de 11h à 12h

Les objectifs de cette semaine sont :
- Faire connaissance 
    - Qui participe
    - Quelles sont les motivations
    - Quelles compétences sont présentes
    - Quels point d’intérêts (technique et culturelle par rapport calcul/stockage/cloud)
- Connaître le projet
    - Historique du projet
    - Quels sont les objectifs (passé, présent, futur) ?
    - État actuel
        - organisation, modèle conceptuel de fonctionnement
        - code (où ? comment ? pourquoi ?)
        - mode de financement
    - Particularités du projet 
- Fixer ensemble les objectifs généraux
- Se répartir en groupes de travail
    - Définir les groupes, leurs objectifs et modes de fonctionnement
    - Définir les rôles (animation, gestion des issues, communication inter-groupes) dans les groupes
    - Identifier les ressources manquantes
    - S’accorder et se familiariser avec les outils de travail et les modes de communication

## Quelques pistes de travail
### Homepage du portail
* quelles informations mettre en avant
* comment organiser les informations

### Outil d'Aide à la Décision (OAD)
* Ergonomie, expérience utilisateur (UX)
    * fonctionnalités pour qui et comment
    * version basique/avancée
    * mélanger calcul, stockage et cloud ?
* Quelles informations sont nécessaires pour définir
    * user
    * infra
    * besoins
    * ontologie
* Quelle API
* Quelle moteur de sélection
* Backend
    * Workflow (git/CI-CD/Hugo)
    * Quelle structure de fichier (faut-il remettre en cause la structure)
        * json-LD https://www.w3.org/TR/2020/CR-json-ld11-20200417/
        * json
        * OWL 
        * RDF 

* Documentation
    * Vocabulaire
    * Notions fondamentales
    * Uses cases
    * How to
    * Générateur de scripts de soumission de jobs en fonction de la ressource
    * Parcours de compétence sur ressources externes 
* Gouvernance du portail
    * Public visé
    * multilingue ? https://gohugo.io/content-management/multilingual/
    * Finalités du portail, ce que fait le portail et ce qu’il ne fera pas (dans un avenir proche)
    * Qui héberge ?
    * Qui et comment on décide ?
    * Qui maintient ?
    * financement
        * INRAE
        * passage à EOSC
        * Autre

