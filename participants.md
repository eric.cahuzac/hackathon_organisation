# Hackathon / Portail Calcul, Stockage et Cloud

## Organisateurs 
* Alexandre DEHNE GARCIA alexandre.dehne-garcia@inrae.fr
* Tovo RABEMANANTSOA tovo.rabemanantsoa@inrae.fr

Avec l'appui de Lise FRAPPIER, facilitatrice en intelligence collective et Eric MARCHOUX, chargé de communication

## Participants
Se rajouter par ordre alphabétique :
 * BRAUX Emmanuel - emmanuel.braux@imt-atlantique.fr
 * Patrick CHABRIER
 * Jan DROUAUD jan.drouaud@inrae.fr
 * Cédric GOBY
 * Loic HOUDE
 * Jacques LAGNEL jacques.lagnel@inrae.fr unité GAFL, Avignon
 * Gilles MATHIEU gilles.mathieu@inserm.fr
 * Christophe MOISY christophe.moisy@inrae.fr
 * Oana VIGY
