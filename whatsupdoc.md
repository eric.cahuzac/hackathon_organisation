### 2020.06.11 [Axe Documentation](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/documentation) : visio  (9h-10h)
  - répartition des activités
  - [visio ici](https://meet.jit.si/pcsc_doc)

### 2020.06.10 [Axe API](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/api) : [visio](https://meet.jit.si/pcsc_api) (14h-16h)
  - inventaire des API faits chez les autres et faire un démonstrateur pour expliquer une API
  - [voir la section](https://etherpad.in2p3.fr/p/pcsc_axe_api_orga)

### 2020.06.09 [Axe Documentation](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/documentation) : 1ère visio du groupe  (9h-10h)
  - clarifier les objectifs
  - [visio ici](https://meet.jit.si/pcsc_doc)

### 2020.06.08 [Axe Ontologies](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/ontologies) : visio  (13h30-15h)
  - brainstorming et répartition des activités
  - [visio ici](https://meet.jit.si/pcsc_ontologies)
  - [doc en cours](https://etherpad.in2p3.fr/p/pcsc_axe_ontologies_orga)

### 2020.06.08 [Axe Critères](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/criteres) : visio  (11h00)
  - [visio ici](https://meet.jit.si/pcsc_criteres)
  - [doc en cours](https://etherpad.in2p3.fr/p/pcsc_axe_criteres)

### 2020.06.08 [Axe AOD](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/oad) : réunion improvisée (10h30-11h00)
  - voir [le doc de prise de note](https://etherpad.in2p3.fr/p/pcsc_axe_oad_orga) 
  - [visio ici](https://meet.jit.si/pcsc_aod)

### 2020.06.08 [Axe UX](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/ux) : visio point sur les avancées (09h00-10h00)
  - voir la section [@tous](https://etherpad.in2p3.fr/p/pcsc_axe_ux_orga) de recherches individuelles
  - [visio ici](https://meet.jit.si/pcsc_UX)

### 2020.06.08 [Axe Formats](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/formats) : Visio (8h00-9h00)
  - [Doc en cours](https://etherpad.in2p3.fr/p/pcsc_axe_formats_orga)
  - [visio ici](https://meet.jit.si/pcsc_formats)


<hr>
<hr>
<hr>

### 2020.06.05 [Axe API](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/api) (16h)
  - [voir la section](https://etherpad.in2p3.fr/p/pcsc_axe_api_orga)
  - [visio ici](https://meet.jit.si/pcsc_api)

### 2020.06.05 [Axe Critères](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/criteres) : visio  (14h00)
  - [visio ici](https://meet.jit.si/pcsc_criteres)
  - [doc en cours](https://etherpad.in2p3.fr/p/pcsc_axe_criteres)
  - prochaine visio :   Lundi 08/06 à 11h


### 2020.06.05 Démo gitlab et mattermost par @damien.berry (14h-16h)[DELAYED]
  - [inscriptions ici](https://evento.renater.fr/survey/une-introduction-a-gitlab-moyc5oqg)(closes)
  - [visio ici](https://meet.jit.si/pcsc)

### 2020.06.05 Plénière de synthèse de fin de semaine (11h-12h)
  - sur  https://meet.jit.si/pcsc ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#
  - [prise de notes](https://etherpad.in2p3.fr/p/pcsc_point_semaine_2_202006051100)
  - [le support de la présentation](https://forgemia.inra.fr/hackathon-portail-calcul/hackathon_organisation/-/blob/master/Communication/Hackathon_synthese_semaine_2.pdf)

### 2020.06.04 [Axe Critères](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/criteres) : visio  (16h30)
  - [visio ici](https://meet.jit.si/pcsc_criteres)
  - [doc en cours](https://etherpad.in2p3.fr/p/pcsc_axe_criteres)
  - prochaines visio : Vendredi 05/06 à 14h et  Lundi 08/06 à 11h

### 2020.06.04 @lise.frappier : 30min de conseils d'organisation pour chaque groupe (8h30-12h30)
  - votez pour les créneaux de votre choix pour chacun des groupes auxquels vous désirez participer : [https://evento.renater.fr/survey/pcsc_lisehelp_orgaaxesdinteret-ieewqmhm](https://evento.renater.fr/survey/pcsc_lisehelp_orgaaxesdinteret-ieewqmhm)
  - les créneaux se suivent en continu, merci de vous connecter à l'heure précise de votre groupe
  - sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#
  - En dehors de ce créneau, vous pouvez toujours [poser vos questions à Lise](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/organisation) à tout moment.

Voici le programme d'accompagnement de @lise.frappier à l'organisation des groupes sur https://meet.jit.si/pcsc :
  - 08h30-09h00   Axe [OAD](https://etherpad.in2p3.fr/p/pcsc_axe_oad_orga)
  - 09h30-10h00   Axe [Documentation](https://etherpad.in2p3.fr/p/pcsc_axe_documentation_orga)
  - 10h30-11h00   Axe [UX](https://etherpad.in2p3.fr/p/pcsc_axe_ux_orga)
  - 11h30-12h00   Axe [Critères](https://etherpad.in2p3.fr/p/pcsc_axe_criteres_orga)
  - 12h00-12h30   Axe [Formats de fichiers](https://etherpad.in2p3.fr/p/pcsc_axe_formats_orga)
  - 08h30-09h00 (vendredi) Axe [Ontologies](https://etherpad.in2p3.fr/p/pcsc_axe_ontologies_orga)

Le groupe [UX](https://etherpad.in2p3.fr/p/pcsc_axe_ux_orga) n'a pas trouvé de créneau compatible.

Le groupes [Chatbot](https://etherpad.in2p3.fr/p/pcsc_axe_chatbot_orga), [Outils pour renseigner les données](https://etherpad.in2p3.fr/p/pcsc_axe_outils-renseigner-donnees_orga) et [API](https://etherpad.in2p3.fr/p/pcsc_axe_ontologies_orga) n'ont pas encore décollé.

<hr>

### 2020.06.03 : 60 minutes de visio brise glace sur l'axe UX (16h-17h)
  - Je propose qu'on discute ; tour de table de présentation et on parle un peu de la suite
  - sur  [https://meet.jit.si/pcsc_ux](https://meet.jit.si/pcsc_ux)
  - prise de notes : [https://etherpad.in2p3.fr/p/pcsc_ux_202006031600](https://etherpad.in2p3.fr/p/pcsc_ux_202006031600)

### 2020.06.03 60 minutes de visio brise glace sur l'axe Critères (14h-15h)
  - Je propose qu'on discute ; tour de table de présentation et on parle un peu de la suite
  - sur  [https://meet.jit.si/pcsc_criteres](https://meet.jit.si/pcsc_criteres)
  - prise de notes : [https://etherpad.in2p3.fr/p/pcsc_criteres_202006031400](https://etherpad.in2p3.fr/p/pcsc_criteres_202006031400)

`Ce que nous avons fait aujourd'hui : réunion brise glace et deffrichage de 1h40 avec tour de table et "pourquoi je suis là" . Puis discussion sur la signification des différents points à traiter. Après avoir convergé sur les définitions, nous avons commencé une liste assez large de critères. Cette liste sera affinée et complétée sur un second etherpad en petit groupe (par affinité vis à vis des domaines) avec 3 créneaux de visio pour échanger sur l'avancement de la rédaction). `


### 2020.06.02 Plénière: On répond à toutes vos questions (10h-12h30 et 14h-18h)
Venez discuter avec nous des périmètres et objectifs possibles des axes d'intérêts, de ce que vous pourriez apporter dans ces axes, de l'organisation en général et surtout de vos groupes...
  - sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#
  - prise de notes : [https://etherpad.in2p3.fr/p/pcsc_help_202006021000](https://etherpad.in2p3.fr/p/pcsc_help_202006021000)


### 2020.06.02 Plénière: Conseils de Lise sur l'auto-organisation des groupes (09h-10h)
  - sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#
  - [les slides sont disponibles !](https://forgemia.inra.fr/hackathon-portail-calcul/hackathon_organisation/-/blob/master/Communication/Hackathon_orga_S2_Mardi.pdf)
  - [Prise de notes](https://etherpad.in2p3.fr/p/pcsc_lisehelp_202006020900)
  - En dehors de ce créneau, vous pouvez toujours [poser vos questions à Lise](https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/organisation) à tout moment.

`En réunion d'organisation, il a été décidé de se laisser jusque mercredi soir pour définir les objectifs. 
Chacun peut se positionner sur un ou plusieurs groupes et en changer par la suite en fonction de l'évolution. 
De même, les objectifs ne sont pas figés dans le marbre, ils pourront donc évoluer au fur et à mesure des avancées et des réflexions.`

### 2020.05.29 Plénière de synthèse de fin de semaine (11h-12h)
sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#

Prise de notes : [https://etherpad.in2p3.fr/p/pcsc_point_semaine_1_202005291100](https://etherpad.in2p3.fr/p/pcsc_point_semaine_1_202005291100)

Le PDF de la présentation :  [https://forgemia.inra.fr/hackathon-portail-calcul/hackathon_organisation/-/blob/master/Communication/Hackathon_synthese_semaine_1_V1.pdf]( https://forgemia.inra.fr/hackathon-portail-calcul/hackathon_organisation/-/blob/master/Communication/Hackathon_synthese_semaine_1_V1.pdf)

  * résumé de la semaine
    * retours sur le kick-off
    * retour sur les sessions de brainstoming
  * proposition d'axes de participation
  * organisaiton de la semaine 2

#### Les Axes d'intérêts proposés :

##### Documentation 
  - Comment structurer la documentation ? 
  - Quelles informations y placer ?
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/documentation
  - https://etherpad.in2p3.fr/p/pcsc_axe_documentation_orga

##### Moteur OAD 
  - Apprentissage automatique de l’OAD à partir des fichiers de données.
  - Intégrer de la recherche libre et une carte interactive.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/oad
  - https://etherpad.in2p3.fr/p/pcsc_axe_oad_orga

##### UX User eXperience
  - Comment PCSC rendre opérationnel, intuitif et adapté à tous.
  - Un de peu design, c'est toujours bien aussi.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/ux
  - https://etherpad.in2p3.fr/p/pcsc_axe_ux_orga

##### Critères 
  - Quels sont les bons critères pour définir les 
    - utilisateurs
    - besoins
    - ressources (infra, services, accès)
    - recommandations ...
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/criteres
  - https://etherpad.in2p3.fr/p/pcsc_axe_criteres_orga

##### Ontologies 
  - Trouver/définir les vocabulaires de référence pour les critères et leurs liens entre eux et les concepts.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/ontologies
  - https://etherpad.in2p3.fr/p/pcsc_axe_ontologies_orga

##### Formats des données
  - Comment structurer les fichiers de données ?
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/formats
  - https://etherpad.in2p3.fr/p/pcsc_axe_formats_orga

##### Outils pour renseigner les données
  - Faciliter le travail de renseignement des données par les fournisseurs de ressources.
  - Les outils peuvent être graphiques ou non.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/outils-renseigner-donnees
  - https://etherpad.in2p3.fr/p/pcsc_axe_outils-renseigner-donnees_orga

##### Chatbot
  - Accompagnement plus simple pour les utilisateurs.
  - Recueil des questions des utilisateurs.
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/chatbot
  - https://etherpad.in2p3.fr/p/pcsc_axe_chatbot_orga

##### API
  - Interagir avec d’autres portails, avec l’extérieur.
  - Pull / Push
  - https://team.forgemia.inra.fr/hackathon-portail-calcul/channels/api
  - https://etherpad.in2p3.fr/p/pcsc_axe_api_orga


### 2020.05.28 Brainstorm & Aide/support aux outils (9h-12h et 14h-18h)
sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#

| créneaux | prises de notes |
| ------ | ------ |
| 28 mai 9h-12h | [https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005280900](https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005280900) |
| 28 mai 14h-18h | [https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005281400](https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005281400) |


### 2020.05.27 Brainstorm & Aide/support aux outils (9h-19h)

sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : +33.1.87.21.0005, PIN: 2258 2211 10#

| créneaux | prises de notes |
| ------ | ------ |
| 27 mai 9h-13h | [https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005270900](https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005270900) |
| 27 mai 13h-19h | [https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005271300](https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005271300) |

### 2020.05.26 Brainstorm & Aide/support aux outils (9h-19h)

sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : Dial-in: +33.1.87.21.0005 PIN: 2258 2211 10#

| créneaux | prises de notes |
| ------ | ------ |
| 26 mai 9h-13h | [https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005260900](https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005260900) |
| 26 mai 13h-19h | [https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005261300](https://etherpad.in2p3.fr/p/pcsc_brainstorming_202005261300) |


### 2020.05.25 Kick-off 

sur  [https://meet.jit.si/pcsc](https://meet.jit.si/pcsc) ou par téléphone : Dial-in: +33.1.87.21.0005 PIN: 2258 2211 10#

| créneaux | prises de notes |
| ------ | ------ |
| 9h-10h | [https://etherpad.in2p3.fr/p/pcsc_kickoff_202005250900](https://etherpad.in2p3.fr/p/pcsc_kickoff_202005250900) |
| 11h-12h | [https://etherpad.in2p3.fr/p/pcsc_kickoff_202005251100](https://etherpad.in2p3.fr/p/pcsc_kickoff_202005251100) |
| 14h-15h | [https://etherpad.in2p3.fr/p/pcsc_kickoff_202005251400](https://etherpad.in2p3.fr/p/pcsc_kickoff_202005251400) |
| 17h-18 | [https://etherpad.in2p3.fr/p/pcsc_kickoff_202005251700](https://etherpad.in2p3.fr/p/pcsc_kickoff_202005251700) |
