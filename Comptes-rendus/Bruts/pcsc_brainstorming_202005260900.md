# PCSC Brainstorm
26 mai 2020 - 9h_13h

## Présents :
- Mikael Loaec
- Christophe Moisy
- Cyril Jousse
- Nadia Ponts
- Fred de Lamotte

## Recueil des idées (sujet, partie, domaine)
Différents parcours dans l'OAD : parcours expert, par profil d'utilisateur/usage, catalogue

ex: traitement de données avec beaucoup de données en entrée et/ou sorties (données climatiques, télédétection, lancement de modèles)

dans le cas de calculs hors du cadre de recherche (pour utilisateurs, diffusion de résultats de recherche pour des acteurs économiques), quelle possibilité ?

Formulaire recherche simple + avancée

Options partie logicielle dans le portail

Outils pré-installés sur les plateformes (+version) : librairies installées

utilisation possible de conteneurs (types singularity) et/ou image machine virtuelle

possibilité d'installer des librairies au niveau utilisateur

3 gros boutons : choix primaries : matériel / logiciel / utilisation

clic : déplie les items correspondants 

matériel : portail actuel

logiciel : librairies + logiciels + versions + conteneurs 

? : protocoles supportés : S3, NFS, ssh, pour clusters 

utilisation : génomique, mécanique des fluides, traitement de données, etc.

possibilité de comparer les résultats de recherche + afficher seulement les différences

prise en compte du profil utilisateur pour savoir si les solutions sont payantes ou non

choix gros grain : prix/ payant ou non

bison futé du taux d'occupation/disponibilité des clusters

chaque plateforme doit remonter des informations sur l'occupation de leurs queues et sur la disponibilité à terme

RGPD compliance des plateformes

Certification gestion des données médicales

Certification IBISA, trust & co. 

Review et retex sur les plateformes

Ajouter un lieu d'échange (tips & tricks, retours, publis...)

Exemple : https://marketplace.eosc-portal.eu/services/c/compute?q=&service_id=&sort=_score

Pour utiliser le portail, il faut que les personnes soient data aware

Facturer l'usage du portail au privé pour financer l'expertise ?

Les tutelles fournissent des ressources en ETP au projet

Ne pas user les gens impliqués.

Quelle infra, quels RH ? Répartir entre les institutions pour limiter les coûts pour chacun.


**point de vigilance :** quid de la mise jour des données sur le portail (Automatique vs manuelle) 

Liens vers les chartes d'utilisation et modèles économiques ?

Pas seulement des liens vers des infras; il faudrait créer du lien (échange de tips, publis, projets etc.)

## UX
Feedback sur les réponses de l'OAD (pertinence des réponses ...)
Feedback global des utilisateurs (retour d'expertise)

Ajout d'un champ tag optionnel dans json ? chat bot ? possibilité de poser des questions sans quitter la page
explication de terme

En alternative d'un chat bot, faire un système de queries fléchées questions/réponses avec menus déroulants (ou autre) pour guider sur les outils à utiliser pour accomplir une tâche ; e.g., un utilisateur veut faire une étude transcriptomique sur un eucaryote (taille du génome) avec 3 répétitions biologiques en utilisant des reads Illumina, que doit-il chercher?

Monétisation des services pour responsabiliser les utilisateurs ?


Faire un roadmap du projet pour savoir où on va (MVP, V.1, V.2 ? ... ne pas tout faire d'un coup, prioriser, cycle de dev du portail ? vie du portail => comment faire demande d'ajout de fonctionnalités au cours de la vie du portail ?)