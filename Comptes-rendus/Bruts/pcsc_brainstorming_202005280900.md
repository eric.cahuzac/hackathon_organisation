# PCSC Brainstorm
28 mai 2020 - 9h-12h

## Présents :
   - Mikael Loaec (adminsys)
   - Willy Bienvenut (bioinfo Moulon) en recherche de ressources, IFB ne correspond pas
   - Emilie Lerigoleur (géo/env geomatique Toulouse, réseaux des observatoires hommes-milieux LabEx DRIIHM (CNRS-INEE), intéressées par le cycle de vie de la donnée, partage de la données, science ouverte, FAIRisation des données, entrepôts de données, stockage/calcul/archivage, brainstorming sur e-infra)
   - Emmanuel Braux (IMT Atlantique Brest, gère un PF openstack, DSI, en accompagnement aux chercheurs sur les outils d'enseignement -> cloud, docbook    - Florian Trincal (DSI INRAE, openstack)
   - Nadia Ponts (INRAE bio épigénomique / bioinfo Bx, end-user, analyse big-data)


## Remarques / warnings
- attention à ne pas tout focaliser sur que de la technique, se placer au niveau du end-user
- interface difficile d'accès pour le end-user néophyte
- rex : passage du local au distant difficile pour le end-user (EB), accompagnement important

   * passer du temps avec les utilisateurs pour bien connaitre leur pratiques pour pouvoir les accompagner
           * -> long mais indispensable, ne avoir une approche trop technique, "descendre" à son niveau
           * -> il faut leur faire faire le premier pas, ça ne viendra pas des end-user tous seuls- parler des outils/méthodes pour travailler sur des ressources distantes
- très important de soigner la communication
- parler aux users avec leurs mots
- danger à trop structurer la doc, passer par des mot clés, des cas usages
- bien comprendre les différents publiques de PCSC (différents niveaux)
- soigner l'UX de la doc et aussi doc/OAD
- la priorité est les néophytes (ceux qui ont le plus besoin) et non les experts
- les experts seront plus intéressés par l'OAD
- Comment impliquer les experts dans l'accompagnement aux néophytes

   * l'expert explique mais on le soulage du travail de rédaction afin qu'il "donne" plus facilement
   * l'expert est plus enclin à donner des infos à ses pairs qu'à un inconnu ou une personne qui n'est pas de son domaine- REX end-user sur sa découverte de l'info = extrêmement chronophage, décourageant, , 

   * difficultés pour comprendre
       * cluster de calcul
       * ssh
       * containers
       * env virtuels
       * linux
       * message d'erreur abscons 
       * quels outils (pas MS Word pour un script, n'est pas une évidence pour le non-informaticien)
   * n'est pas un problème quand on est "jeune" :
       * matériel : coeur, ram, ...- connaitre ce que veut faire le end-user est finalement assez difficile, lui-même ne sait pas toujours
- être capable de reformuler la demande du néophyte est une difficulté en soit
- néophytes pas toujours réceptifs (comment avancer ?)
- proposer le référencement des entrepôts
- le portail doit dédramatiser les aspects calcul/stockage/cloud, il faut faire tomber la peur de ces outils
- aller vers l'autonomie des utilisateurs = Knowledge Management System


## Recueil des idées (sujet, partie, domaine)
- important d'avoir rex pour s'améliorer mais aussi de diffuser les rex pour promouvoir l'outils. On fait confiance à ses pairs

   * webcast, interview écrite, webinaires flash, tutos, TP d'utilisation, classe virtuelle- intégrer des néophytes comme beta-testeurs
- construire un portfolio via des cas d'usage (entrée) vers des rex
- chatbot (même temporaires pour co-construire : 

   * permet d'avoir des questions réelles des end-users
   * il faudrait des humains derrière (au moins durant un temps de test/construction)
   * construire une base de connaissance
- recenser les questions des utilisateurs : connaitre leurs problèmes, connaitre leur vocabulaire
- identifier des key-users et les interroger 
- définir les termes : exemple cloud, container, machine virtuelle
- proposer un forum d'entre-aide
- proposer un canal mattermost : dynamique, interactif
- si questions publique : honte de s'exprimer devant les autres 
- construire mooc ? mooc existants ?
- créer une liste de formations curées (à présenter sous forme de parcours de compétences ?)  
- doc step by step, progressivité, ne pas montrer la montagne à apprendre, la longueur et la pente du chemin à parcourir
- expliquer l'intérêt d'un cluster de calcul ou d'infra distantes / local
- expliquer comment gérer ses données, son stockage, gérer des gros jeux de données, outils adaptés
- proposer un outils qui montre le temps de calcul sur un portable, sur une workstation, des clusters tier3/2/1/0/ , la grille

   * que ce soit automatique par défaut dans l'OAD ?- trois étapes pour réussir :

       1. écouter le besoin du end-user
       2. traduire le besoin du user grâce l'expertise de l'accompagnant
       3. traduire le besoin en technique pure du besoin- lancer une enquête de uses-cases ? 

   * sinon demander aux :
       *  correspondants
       *  informaticiens d'unités de recherche
   * - danger :
       * peur du flicage
       * peur de l'avis de l'autre
- Mettre un mécanisme de check du last update des descriptions des infra pour pouvoir relancer les responsables pour vérifier la cohérence
Critères :
    - temps de vie des données (engagement des infras sur la rétention)
