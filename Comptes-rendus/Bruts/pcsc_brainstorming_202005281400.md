# PCSC Brainstorm
28 mai 2020 - 14h-18h

## Présents :
    - Arnaud JEAN-CHARLES (chargé de projet web/donné Labex DRIIHM, co-porteur du projet SoDRIIHM=PF pour faciliter la gestion de la donnée et FAIRisation, centralisation de l'information utiles aux chercheurs), intéressé par 1) les ontologies + schéma de données et 2) API
    - Paul Rénaudot : Engie, Etudiant X, Comment donner une infra aux chercheurs une infra afin que les chercheurs n'aient pas à implémenter leurs propres infra)
    - Daniel Salas, INSERM, intéressé par le chabot : aide au user, interface, code et fonctionnement
    - David Benaben (cbib)
    - Pascal Voury (GENCI), intéressé par les critères des infra 


## Remarques/warnings
FEEDBACK hackathon :

   * tout le monde pouvait parlé durant le kick-off : bien
   * prez clair
   * la période brainstorming est bien pour pouvoir savoir ce que chacun peut-apporter = rassurant
   * première impression sur le gitlab : toufu, lecture difficile au milieu de tous ces liens mais kick-off aide à mieux comprendre le tout
   * entrée en douceur dans le projet est appréciée : passage d'observateur à acteur assez facile
   * motivation : venu par connaissance des organisateurs, bon projet, super flyer
   * kick-off réparti en plusieurs sessions =  très bien, souple
	   - ambiance décontractée, tour de table permet à tout le monde de s'exprimer facilement 
	   - prez claire
	   - que va sortir du hackathon, du brainstorming 
	   - difficile de savoir ce qui va en sortir 
- mettre un API en requête
- quid des entreprises qui proposent des infra
- attention difficile de spécifier les critères en stockage (bcp de terme + ou - recouvrant et chacun à sa *)
- les mesocentres sont plus compliqués à décrire que les infras GENCI plus homogènes
- attention à arriver à un stade opérationnel 


## Recueil des idées (sujet, partie, domaine)
- mettre en place une API afin que d'autres portails d'aide aux chercheurs puissent regrouper les infos
- lien vers les ontologies : \url{https://lov.linkeddata.es/dataset/lov/terms?q=cpu}
- ontologies pour les personnes : \url{http://xmlns.com/foaf/spec/} et pour les relations : \url{https://vocab.org/relationship/}
- permettre au fournisseur de ressources de remplir ses caractéristiques via un outil graphique :
     - normé
     - facile à remplir
     - visuel
     - facile de se rendre compte si la description correspond bien à l'infra/services
     - ex :  graph rdf (\url{https://dist.neo4j.com/wp-content/uploads/20170617195358/RDF-graph-example-graphconnect.png?)} -> schema json rdf
     /!\ attention ça peut devenir une usine gaz 
- usage SKOS ou OWL ? utile d'aller jusque là ?
- outils : Protégé : \url{https://protege.stanford.edu/} (version web possible : \url{https://protegewiki.stanford.edu/wiki/WebProtegeUsersGuide)}
- rex Daniel : faire tourner un pipeline de bio (venue de comput canada) : impossible de le faire tourné sur x+ infras car toujours des contraintes : ouvertures de ports, télécharger des FSvirtuel, ... Seul le cloud google a réussi à faire tourner le pipeline
... 
- mettre en place un chatbot :

    	- interface plus simple que d'aller fouiller dans un doc immense- à la fin demander si le user est content et si non alors lui demander son avis (chatbot ou site) : note via des étoiles + texte libres possible





### Critères : 
    - prendre en compte la bande passante en fonction du jeu de données et les distances user/infra
    - type de données (texte (génome) binaires (séquençage ...)
    - thématique
    - niveau de droits, liberté d'installation
    - ouverture de ports possible
    - quels ports ouverts
    - compilateurs
     - critères d'acceptation de l'accès aux infras

   * géographie (région, ville, zones, ou GPS tout simplement)
       * financement français
       * recherche publique avec publication des à la clef 
       * le projet a-t-il un DMP (publique / pas publique) 
   * aspect environnementaux (efficacité énergétique, empreinte carbone) et sociétaux (politique personnes à handicap, stages, CDD/CDDI, ...), \url{https://ecoinfo.cnrs.fr/}
   * DMP 
   * prix
       * prix à payer par le end-user
       * vrai prix (coût environné (bât, fluide, )par heure de calcul, par To, etc)
       * coût du réseaux (Renater) du transfert entre le user et l'infra (aller/retour), 
       * coût des E/S
       * prix stockage
           * rapide
           * lent
           * capacitif
           * object/filer      - service de multitenant sur 

   * équivalent CO2 du besoin (en moyenne ou par infra), en électricité en Watts, + équivalent en km avec une voiture
   * hébergement web (galaxy, et autre visualiseurs, ex : genombrowser)
   * banques de données
   * service de base données à la demande
   * infras spark, hadoop : HPDA    
    


## Extrait d'infos recueillis par l'Institut Français de Bioinformatique (IFB) auprès des plateformes fédérées

Plateforme (nom)

Email de contact (direction)

Région

Ville

Main affiliation (CNRS/INRAE/etc)

Institute / University

Current site (lieu d'hébergement de l'infra)

Cluster / Cloud

Contact email for budget

Nom des ressources ou services (exemple: cluster, cloud, service web)

Types de services assurés sur cette infra (Analyse de données pour des usagers, Hébergement de comptes usagers (calcul et stockage), Formation, Développement de ressources logicielles (outils, DB) originales, Déploiement de ressources web (outils, DB), Autres types de service (préciser), Remarques)

### Équipements:
- Cœurs CPU (sans hyperthread)
- Lames GPU
- Stockage rapide (To)
- Stockage capacitif (To)
- Stockage total
- Sauvegarde sur bande ou autre type de matériel (To)
- Besoins en heures CPU (quand l'infra n'est pas gérée par la plateforme)

### Finance
- Dépenses équipement de l'année
- Coût directs hébergement (fluides, RH ASR, sécurité, ...)
- Fonctionnement hors hébergement de l'infra

### Personnel        
- ETP permanents associés aux services
- ETP CDD associés au service
- ETP permanents idéaux pour assurer les services
- Répartition ETP: Analyse de données pour des usagers, Hébergement de comptes usagers, Formation, Développement de ressources logicielles (outils, DB) originales, Déploiement de ressources web (outils, DB), Autres types de service (préciser)

Coût

Coût unitaire (Coût environné par unité en absorbant les autres coûts: personnel, amortissement, maintenance, hébergement)

Coût du calcul CPU (pour 1000H)

Coût du calcul GPU

Coût stockage capacitif

Coût stockage performant

Coût stockage mixte

Usage

Nombre d'utilisateurs actifs

Heures de calculs

...
